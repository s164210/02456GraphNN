import torch
from chemprop.models.distributions_pytorch import VariationalPosterior
from chemprop.nn_utils import index_select_ND
td = torch.distributions
from chemprop.features.featurization import get_atom_fdim
from torch.nn.parameter import Parameter


class VarDense(torch.nn.Module):
    def __init__(self, in_dim, out_dim, prior, init, bias, device, **kwargs):
        super(VarDense, self).__init__(**kwargs)
        self.init = init
        self.in_dim = in_dim
        self.out_dim = out_dim
        self.prior = prior
        self.use_bias = bias
        self.training = False
        self.sampling = False
        self.device = device

        self.weight_mu = Parameter(torch.Tensor(self.out_dim, self.in_dim), requires_grad=True)
        self.weight_rho = Parameter(torch.Tensor(self.out_dim, self.in_dim), requires_grad=True)
        if self.init == 'xavier':
            torch.nn.init.xavier_uniform_(self.weight_mu)
        else:
            torch.nn.init.zeros_(self.weight_mu)
        torch.nn.init.constant_(self.weight_rho, -10)
        self.weight_dist = VariationalPosterior(self.weight_mu, self.weight_rho, self.device)
        if self.use_bias:
            self.bias_mu = Parameter(torch.Tensor(self.out_dim, ), requires_grad=True)
            self.bias_rho = Parameter(torch.Tensor(self.out_dim, ), requires_grad=True)

            torch.nn.init.zeros_(self.bias_mu)
            torch.nn.init.constant_(self.bias_rho, -10)

            self.bias_dist = VariationalPosterior(self.bias_mu, self.bias_rho, self.device)


    def forward(self, inputs):
        self.log_prior = 0
        self.log_variational_posterior = 0
        weight = self.weight_dist.sample(self.training, self.sampling).to(self.device)
        self.log_prior += torch.sum(self.prior.log_prob(weight))
        self.log_variational_posterior += torch.sum(self.weight_dist.log_prob(weight))
        if self.use_bias:
            bias = self.bias_dist.sample(self.training, self.sampling).to(self.device)
            self.log_variational_posterior += torch.sum(self.bias_dist.log_prob(bias))
            self.log_prior += torch.sum(self.prior.log_prob(bias))
            bias = bias.to(self.device)

        out = torch.matmul(inputs, torch.transpose(weight, 0, 1))
        out += bias if self.use_bias else 0
        return out


class BayesianDMPNN(torch.nn.Module):

    def __init__(self, T, hidden_size, h_i, prior, dropout_prob, init, device, **kwargs):
        super(BayesianDMPNN, self).__init__(**kwargs)
        self.T = T
        self.prior = prior
        self.hidden_size = hidden_size
        self.init = init
        self.device = device

        self.W_i_mu = Parameter(torch.Tensor(hidden_size, h_i), requires_grad=True)
        self.W_i_rho = Parameter(torch.Tensor(hidden_size, h_i), requires_grad=True)
        self.W_a_mu = Parameter(torch.Tensor(hidden_size, hidden_size), requires_grad=True)
        self.W_a_rho = Parameter(torch.Tensor(hidden_size, hidden_size), requires_grad=True)
        self.W_m_mu = Parameter(torch.Tensor(hidden_size, hidden_size + get_atom_fdim()), requires_grad=True)
        self.W_m_rho = Parameter(torch.Tensor(hidden_size, hidden_size + get_atom_fdim()), requires_grad=True)

        if self.init == 'xavier':
            torch.nn.init.xavier_uniform_(self.W_i_mu)
            torch.nn.init.xavier_uniform_(self.W_a_mu)
            torch.nn.init.xavier_uniform_(self.W_m_mu)
        else:
            torch.nn.init.zeros_(self.W_i_mu)
            torch.nn.init.zeros_(self.W_a_mu)
            torch.nn.init.zeros_(self.W_m_mu)
        torch.nn.init.constant_(self.W_i_rho, -10)
        torch.nn.init.constant_(self.W_a_rho, -10)
        torch.nn.init.constant_(self.W_m_rho, -10)

        self.W_i_dist = VariationalPosterior(self.W_i_mu, self.W_i_rho, self.device)
        self.W_a_dist = VariationalPosterior(self.W_a_mu, self.W_a_rho, self.device)
        self.W_m_dist = VariationalPosterior(self.W_m_mu, self.W_m_rho, self.device)
        self.act_func = torch.nn.ReLU()
        self.dropout_layer = torch.nn.Dropout(p=dropout_prob)

        self.training = False
        self.sampling = False
        self.built = True


    def forward(self, batch_mol):
        f_atoms, f_bonds, a2b, b2a, b2revb, a_scope, b_scope = batch_mol.get_components()
        f_atoms, f_bonds, a2b, b2a, b2revb = f_atoms.to(self.device), f_bonds.to(self.device), a2b.to(self.device), b2a.to(self.device), b2revb.to(self.device)

        W_i = self.W_i_dist.sample(self.training, self.sampling)
        W_a = self.W_a_dist.sample(self.training, self.sampling)
        W_m = self.W_m_dist.sample(self.training, self.sampling)

        W_i, W_a, W_m = W_i.to(self.device), W_a.to(self.device), W_m.to(self.device)

        # If using mean of every variational distribution delete reference to parameter
        if not self.sampling and not self.training:
            W_i, W_a, W_m = W_i.detach(), W_a.detach(), W_m.detach()

        self.log_prior = torch.sum(self.prior.log_prob(W_i))
        self.log_prior += torch.sum(self.prior.log_prob(W_a))
        self.log_prior += torch.sum(self.prior.log_prob(W_m))
        self.log_variational_posterior = torch.sum(self.W_i_dist.log_prob(W_i))
        self.log_variational_posterior += torch.sum(self.W_a_dist.log_prob(W_a))
        self.log_variational_posterior += torch.sum(self.W_m_dist.log_prob(W_m))

        input = torch.matmul(f_bonds, torch.transpose(W_i, 0, 1))  # num_bonds x hidden_size
        message = self.act_func(input)  # num_bonds x hidden_size

        for t in range(self.T):
            nei_a_message = index_select_ND(message, a2b)  # num_atoms x max_num_bonds x hidden
            a_message = nei_a_message.sum(dim=1)  # num_atoms x hidden
            rev_message = message[b2revb]  # num_bonds x hidden
            message = a_message[b2a] - rev_message  # num_bonds x hidden

            message = torch.matmul(message, torch.transpose(W_a, 0, 1))
            message = self.act_func(input + message)  # num_bonds x hidden_size
            message = self.dropout_layer(message)  # num_bonds x hidden
        a2x = a2b
        nei_a_message = index_select_ND(message, a2x)  # num_atoms x max_num_bonds x hidden
        a_message = nei_a_message.sum(dim=1)  # num_atoms x hidden
        a_input = torch.cat([f_atoms, a_message], dim=1)  # num_atoms x (atom_fdim + hidden)
        atom_hiddens = torch.matmul(a_input, torch.transpose(W_m, 0, 1))
        atom_hiddens = self.act_func(atom_hiddens)

        mol_vecs = []
        for i, (a_start, a_size) in enumerate(a_scope):
            cur_hiddens = atom_hiddens.narrow(0, a_start, a_size)
            mol_vec = cur_hiddens  # (num_atoms, hidden_size)
            mol_vec = mol_vec.sum(dim=0) / a_size
            mol_vecs.append(mol_vec)
        mol_vecs = torch.stack(mol_vecs, dim=0)

        return mol_vecs