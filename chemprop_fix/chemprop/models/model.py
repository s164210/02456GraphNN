from rdkit import Chem
import torch
import torch.nn as nn



from typing import List, Union


from chemprop.args import TrainArgs
from chemprop.features import BatchMolGraph
from torch.nn.parameter import Parameter
from chemprop.models.layers import VarDense, BayesianDMPNN
from chemprop.features.featurization import get_atom_fdim, get_bond_fdim
td = torch.distributions


import numpy as np
from rdkit import Chem
import torch
import torch.nn as nn

#from .mpn import MPN
from .mpn import MPNEncoder
from chemprop.args import TrainArgs
from chemprop.features import BatchMolGraph
from chemprop.nn_utils import get_activation_function, initialize_weights


class BayesianMoleculeModel(torch.nn.Module):
    """A :class:`MoleculeModel` is a model which contains a message passing network following by feed-forward layers."""

    def __init__(self, args, device, featurizer: bool = False):
        """
        :param args: A :class:`~chemprop.args.TrainArgs` object containing model arguments.
        :param featurizer: Whether the model should act as a featurizer, i.e., outputting the
                           learned features from the last layer prior to prediction rather than
                           outputting the actual property predictions.
        """
        super(BayesianMoleculeModel, self).__init__()

        self.softplus = torch.nn.Softplus(beta=1)
        self.classification = args.dataset_type == 'classification'
        self.multiclass = args.dataset_type == 'multiclass'
        self.featurizer = featurizer
        self.init = args.init

        self.output_size = args.num_tasks
        if self.multiclass:
            self.output_size *= args.multiclass_num_classes

        if self.classification:
            self.sigmoid = nn.Sigmoid()

        if self.multiclass:
            self.multiclass_softmax = nn.Softmax(dim=2)

        self.target_dim = args.num_tasks
        self.task_type = args.dataset_type
        self.eps = torch.tensor(1e-8)
        self.device = device
        self.create_encoder(args)
        self.create_ffn(args)
        self.create_ffn_mean(args)
        if self.task_type == 'regression':
            self.create_ffn_var(args)

    def create_encoder(self, args: TrainArgs) -> None:
        self.encoder = BayesianDMPNN(args.depth, args.hidden_size, get_bond_fdim(), args.prior, args.dropout, args.init, self.device)

    def create_ffn(self, args: TrainArgs) -> None:
        self.multiclass = args.dataset_type == 'multiclass'
        if self.multiclass:
            self.num_classes = args.multiclass_num_classes
        if args.features_only:
            first_linear_dim = args.features_size
        else:
            first_linear_dim = args.hidden_size * args.number_of_molecules
            if args.use_input_features:
                first_linear_dim += args.features_size

        if args.atom_descriptors == 'descriptor':
            first_linear_dim += args.atom_descriptors_size

        dropout = torch.nn.Dropout(args.dropout)
        activation = torch.nn.ReLU()

        # Create FFN layers
        if args.ffn_num_layers == 1:
            ffn = [
                dropout,
                VarDense(first_linear_dim, self.output_size, args.prior, args.init, bias=True, device=self.device)
            ]
        else:
            ffn = [
                dropout,
                VarDense(first_linear_dim, args.ffn_hidden_size, args.prior, args.init, bias=True, device=self.device)
            ]
            for _ in range(args.ffn_num_layers - 2):
                ffn.extend([
                    activation,
                    dropout,
                    VarDense(args.ffn_hidden_size, args.ffn_hidden_size, args.prior, args.init, bias=True, device=self.device),
                ])

        # Create FFN model
        self.ffn = torch.nn.Sequential(*ffn)

    def create_ffn_mean(self, args: TrainArgs) -> None:
        dropout = torch.nn.Dropout(args.dropout)
        activation = torch.nn.ReLU()
        ffn = [
            activation,
            dropout,
            VarDense(args.ffn_hidden_size, self.output_size, args.prior, args.init, bias=True, device=self.device)
        ]
        self.ffn_mean = torch.nn.Sequential(*ffn)

    def create_ffn_var(self, args: TrainArgs) -> None:
        dropout = torch.nn.Dropout(args.dropout)
        activation = torch.nn.ReLU()
        ffn = [
            activation,
            dropout,
            VarDense(args.ffn_hidden_size, args.num_tasks, args.prior, args.init, bias=True, device=self.device)
        ]
        self.ffn_var = torch.nn.Sequential(*ffn)

    def sampling_mode(self, sampling):
        self.encoder.sampling = sampling
        for mod in list(self.modules())[0].ffn:
            if isinstance(mod, VarDense):
                mod.sampling = sampling

        for mod in list(self.modules())[0].ffn_mean:
            if isinstance(mod, VarDense):
                mod.sampling = sampling
        if self.task_type == 'regression':
            for mod in list(self.modules())[0].ffn_var:
                if isinstance(mod, VarDense):
                    mod.sampling = sampling
        self.sampling = sampling

    def training_mode(self, training):
        self.encoder.training = training
        for mod in list(self.modules())[0].ffn:
            if isinstance(mod, VarDense):
                mod.training = training

        for mod in list(self.modules())[0].ffn_mean:
            if isinstance(mod, VarDense):
                mod.training = training
        if self.task_type == 'regression':
            for mod in list(self.modules())[0].ffn_var:
                if isinstance(mod, VarDense):
                    mod.training = training
        self.training = training

    def nll(self, batch, y_true):
        if self.task_type == 'regression':
            self.y_pred, y_rho = self.forward(batch)
            self.y_std = self.softplus(y_rho) + self.eps
        elif self.task_type == 'classification':
            self.y_pred = self.forward(batch)

        if self.task_type == 'regression':
            pred_dist = td.Normal(self.y_pred, scale=self.y_std)
        elif self.task_type == 'classification':
            pred_dist = td.bernoulli.Bernoulli(logits=self.y_pred)
        return -torch.sum(pred_dist.log_prob(y_true))

    def elbo(self, batch, y_true, num_batches, num_samples, kl_weight=None):
        beta = 1 / num_batches if kl_weight is None else kl_weight
        elbo_sum = 0.0
        for s in range(num_samples):
            nll = self.nll(batch, y_true)
            kl = (self.encoder.log_variational_posterior - self.encoder.log_prior)
            for mod in list(self.modules())[0].ffn:
                if isinstance(mod, VarDense):
                    kl += (mod.log_variational_posterior - mod.log_prior)

            for mod in list(self.modules())[0].ffn_mean:
                if isinstance(mod, VarDense):
                    kl += (mod.log_variational_posterior - mod.log_prior)
            if self.task_type == 'regression':
                for mod in list(self.modules())[0].ffn_var:
                    if isinstance(mod, VarDense):
                        kl += (mod.log_variational_posterior - mod.log_prior)
            elbo_sum += nll + beta * kl
        if torch.isnan(nll):
            raise ValueError('nll is nan')
        if torch.isnan(kl):
            raise ValueError('kl is nan')
        if elbo_sum / num_samples > 1e14:
            print(nll, kl)
        return elbo_sum / num_samples

    def forward(self,
                batch: BatchMolGraph,
                features_batch=None,
                atom_descriptors_batch=None) -> torch.FloatTensor:

        x = self.encoder.forward(batch)
        x = self.ffn(x)
        output_mean = self.ffn_mean(x)
        if self.task_type == 'regression':
            output_var = self.ffn_var(x)
        if self.task_type == 'regression':
            return output_mean, output_var
        else:
            return output_mean


class LangvianMoleculeModel(nn.Module):
    """A :class:`MoleculeModel` is a model which contains a message passing network following by feed-forward layers."""

    def __init__(self, args, featurizer: bool = False):
        """
        :param args: A :class:`~chemprop.args.TrainArgs` object containing model arguments.
        :param featurizer: Whether the model should act as a featurizer, i.e., outputting the
                           learned features from the last layer prior to prediction rather than
                           outputting the actual property predictions.
        """
        super(LangvianMoleculeModel, self).__init__()
        self.softplus = torch.nn.Softplus(beta=1)   
        self.classification = args.dataset_type == 'classification'
        self.multiclass = args.dataset_type == 'multiclass'
        self.featurizer = featurizer

        self.output_size = args.num_tasks
        if self.multiclass:
            self.output_size *= args.multiclass_num_classes

        if self.classification:
            self.sigmoid = nn.Sigmoid()

        if self.multiclass:
            self.multiclass_softmax = nn.Softmax(dim=2)

        self.create_encoder(args)
        self.create_ffn(args)

        initialize_weights(self)

        # Initialize prior
        self.prior = td.Normal(0, 1)

        self.create_ffn_mean(args)
        self.create_ffn_var(args)

        self.eps = torch.tensor(1e-8)



    def create_encoder(self, args: TrainArgs) -> None:
        """
        Creates the message passing encoder for the model.
        :param args: A :class:`~chemprop.args.TrainArgs` object containing model arguments.
        """
        self.encoder = MPNEncoder(args, get_atom_fdim(), get_bond_fdim())


    def sum_ll(self, batch, y_true): # log likelihood.
        self.y_pred, y_rho = self.forward(batch)
        self.y_std = self.softplus(y_rho) + self.eps
        pred_dist = td.Normal(self.y_pred, scale=self.y_std)
        ll = torch.sum(pred_dist.log_prob(y_true))
        return ll

    def prior_loss(self):
        loss = 0.0
        for modu in list(self.modules())[0].ffn:
            if isinstance(modu, torch.nn.Linear):
                loss += torch.sum(self.prior.log_prob(modu.weight.data))
                loss += torch.sum(self.prior.log_prob(modu.bias.data))

        for modu in list(self.modules())[0].ffn_mean:
            if isinstance(modu, torch.nn.Linear):
                loss += torch.sum(self.prior.log_prob(modu.weight.data))
                loss += torch.sum(self.prior.log_prob(modu.bias.data))

        for modu in list(self.modules())[0].ffn_var:
            if isinstance(modu, torch.nn.Linear):
                loss += torch.sum(self.prior.log_prob(modu.weight.data))
                loss += torch.sum(self.prior.log_prob(modu.bias.data))

        loss += torch.sum(self.prior.log_prob(self.encoder.W_i.weight.data))
        loss += torch.sum(self.prior.log_prob(self.encoder.W_h.weight.data))
        loss += torch.sum(self.prior.log_prob(self.encoder.W_o.weight.data))

        return loss

    def create_ffn(self, args: TrainArgs) -> None:
        """
        Creates the feed-forward layers for the model.
        :param args: A :class:`~chemprop.args.TrainArgs` object containing model arguments.
        """
        self.multiclass = args.dataset_type == 'multiclass'
        if self.multiclass:
            self.num_classes = args.multiclass_num_classes
        if args.features_only:
            first_linear_dim = args.features_size
        else:
            first_linear_dim = args.hidden_size * args.number_of_molecules
            if args.use_input_features:
                first_linear_dim += args.features_size

        if args.atom_descriptors == 'descriptor':
            first_linear_dim += args.atom_descriptors_size

        dropout = nn.Dropout(args.dropout)
        activation = get_activation_function(args.activation)

        # Create FFN layers
        if args.ffn_num_layers == 1:
            ffn = [
                dropout,
                nn.Linear(first_linear_dim, self.output_size)
            ]
        else:
            ffn = [
                dropout,
                nn.Linear(first_linear_dim, args.ffn_hidden_size)
            ]
            for _ in range(args.ffn_num_layers - 2):
                ffn.extend([
                    activation,
                    dropout,
                    nn.Linear(args.ffn_hidden_size, args.ffn_hidden_size),
                ])
            ffn.extend([
                activation,
                dropout,
                nn.Linear(args.ffn_hidden_size, args.ffn_hidden_size),
            ])

        # Create FFN model
        self.ffn = nn.Sequential(*ffn)

    def create_ffn_mean(self, args: TrainArgs) -> None:
        dropout = torch.nn.Dropout(args.dropout)
        activation = torch.nn.ReLU()
        ffn = [
            activation,
            dropout,
            torch.nn.Linear(args.ffn_hidden_size, self.output_size)
        ]
        self.ffn_mean = torch.nn.Sequential(*ffn)

    def create_ffn_var(self, args: TrainArgs) -> None:
        dropout = torch.nn.Dropout(args.dropout)
        activation = torch.nn.ReLU()
        ffn = [
            activation,
            dropout,
            torch.nn.Linear(args.ffn_hidden_size, self.output_size)
        ]
        self.ffn_var = torch.nn.Sequential(*ffn)


    def forward(self,
                batch: BatchMolGraph,
                features_batch=None,
                atom_descriptors_batch=None) -> torch.FloatTensor:

        x = self.encoder.forward(batch)
        x = self.ffn(x)
        output_mean = self.ffn_mean(x)
        output_var = self.ffn_var(x)
        return output_mean, output_var