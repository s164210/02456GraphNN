import torch
from torch.distributions.mixture_same_family import MixtureSameFamily
td = torch.distributions
device = torch.device('cuda')

class MixturePrior(object):
    def __init__(self, prop, sigma1, sigma2):
        self.prop = prop
        self.sigma1 = sigma1
        self.sigma2 = sigma2
        mu = torch.zeros(2).to(device)
        sigmas = torch.tensor([sigma1, sigma2]).to(device)
        props = torch.tensor([1-prop, prop]).to(device)
        comp = td.Normal(mu, sigmas)
        self.dist = MixtureSameFamily(td.Categorical(props),comp)

    def sample(self):
        return self.dist.sample()

    def log_prob(self, x):
        return self.dist.log_prob(x)

class VariationalPosterior(object):
    def __init__(self, mu, rho, device):
        super().__init__()
        self.device = device
        self.mu = mu
        self.rho = rho
        self.stdNorm = td.Normal(0,1)
        self.softplus = torch.nn.Softplus()
        self.pi = torch.acos(torch.zeros(1)).item() * 2

    @property
    def sigma(self):
        return self.softplus(self.rho)

    def sample(self, training, sampling=True):
        if training:
            epsilon = self.stdNorm.sample((self.rho).shape).to(self.device)
            return self.mu + self.sigma*epsilon
        elif sampling:
            return td.Normal(self.mu, self.sigma).sample()
        else:
            return self.mu

    def log_prob(self, x):
        return td.Normal(self.mu, self.sigma).log_prob(x)

        #torch.sum(-torch.log(2*self.pi)
               #              - torch.log(self.sigma) - ((x - self.mu)**2) / (2*self.sigma**2))
