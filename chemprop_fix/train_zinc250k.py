import pandas as pd
import numpy as np
import matplotlib.pyplot as plt
import os.path
from os import path

from chemprop.data.data import MoleculeDatapoint, MoleculeDataset
from chemprop.data import MoleculeDataLoader, split_data
from chemprop.nn_utils import get_activation_function, initialize_weights, NoamLR
import torch
from chemprop.utils import build_lr_scheduler
td = torch.distributions
from chemprop.args import TrainArgs, HyperoptArgs
from tqdm import tqdm
from chemprop.models.model import BayesianMoleculeModel
from chemprop.data import get_data


class BayesianArgs(TrainArgs):
    prior = td.Normal(0, 1)
    data_path = "data/zinc250k.csv"  # "data/pdbbind_full.csv"
    init = 'zeros'  # Changed from 'xavier' sometimes produces nans :o
    init_lr = 5e-3  # 5e-3
    max_lr = 1e-3  # 1e-2
    final_lr = 1e-4  # 1e-5
    split_sizes = (0.8, 0.1, 0.1)
    train_data_size = 9880 * split_sizes[0]  # 642*split_sizes[0]
    epochs = 1#10000
    warmup_epochs = 2


args = BayesianArgs()
args.num_tasks = 3
args.ffn_hidden_size = 1000
args.hidden_size = 1000
args.ffn_num_layers = 3
args.depth = 6
args.dropout = 0.0  # Changed from 0.15
args.num_workers = 0
args.batch_size = 50  # 200 # Changed from 50
args.dataset_type = 'regression'

load_trained = True
device = torch.device('cuda')
mod = BayesianMoleculeModel(args, device=device)

path_name = args.data_path.partition("/")[2].partition(".")[0]
path_name += f"_hidden_{args.ffn_hidden_size}"
path_name += f"_ffn_num_{args.ffn_num_layers}"
path_name += f"_depth_{args.depth}"
path_name += f"_dropout_{args.dropout}"
path_name += f"_epochs_{args.epochs}"

data = get_data(
        path=args.data_path,
        args=args,
        skip_none_targets=True)
train_data, val_data, test_data = split_data(data=data,
                                             split_type=args.split_type,
                                             sizes=args.split_sizes,
                                             seed=args.seed,
                                             num_folds=args.num_folds,
                                             args=args,
                                             logger=None)


def train(model: BayesianMoleculeModel,
          data_loader: MoleculeDataLoader,
          optimizer,
          args: TrainArgs,
          n_iter) -> float:
    """
    Trains a model for an epoch.

    :param model: A :class:`~chemprop.models.model.MoleculeModel`.
    :param data_loader: A :class:`~chemprop.data.data.MoleculeDataLoader`.
    :param loss_func: Loss function.
    :param optimizer: An optimizer.
    :param args: A :class:`~chemprop.args.TrainArgs` object containing arguments for training the model.
    :param n_iter: The number of iterations (training examples) trained on so far.
    :return: The total number of iterations (training examples) trained on so far.
    """
    model.to(device)
    model.training_mode(True)
    model.sampling_mode(True)
    model.train()
    loss_sum = iter_count = 0
    num_batches = len(data_loader)

    for b, batch in enumerate(tqdm(data_loader, total=len(data_loader), leave=False, disable=True)):
        # Prepare batch
        batch: MoleculeDataset
        mol_batch, features_batch, target_batch, atom_descriptors_batch = \
            batch.batch_graph(), batch.features(), batch.targets(), batch.atom_descriptors()

        target_batch = torch.Tensor(target_batch).to(device)

        kl_weight_batch = (2 ** (num_batches - b)) / (2 ** num_batches - 1)

        # Run model
        model.zero_grad()
        loss = model.elbo(mol_batch[0], target_batch, num_batches, num_samples=1)

        # In case elbo spikes or becomes nan stop training before optimizer steps (for debug)
        if torch.isnan(loss):
            raise ValueError("Loss is nan")

        if loss > 1e11:
            raise ValueError("Loss spiked")

        loss_sum += loss.item()
        iter_count += 1

        loss.backward()
        optimizer.step()

        if isinstance(scheduler, NoamLR):
            scheduler.step()

        n_iter += len(batch)
    return loss_sum / num_batches


# Checkpoint training
save_path = path_name.partition("epochs_")[0] + ".pt"
epochs = args.epochs
optimizer = torch.optim.Adam(params=mod.parameters(), lr=args.init_lr)
scheduler = build_lr_scheduler(optimizer, args)
train_data_loader = MoleculeDataLoader(dataset=train_data, batch_size=args.batch_size, num_workers=args.num_workers,
                                       class_balance=args.class_balance, shuffle=True, seed=args.seed)
val_data_loader = MoleculeDataLoader(dataset=val_data, batch_size=args.batch_size, num_workers=args.num_workers)
test_data_loader = MoleculeDataLoader(dataset=test_data, batch_size=len(test_data), num_workers=args.num_workers)

best_val_loss = 1e30
loss_lst = []
loss_val = []
for n_iter in range(epochs):
    # Training
    loss_sum = train(model=mod,
                     data_loader=train_data_loader,
                     optimizer=optimizer,
                     args=args,
                     n_iter=n_iter)
    loss_lst.append(loss_sum)

    # Validation
    mod.eval()
    batch_val = next(iter(val_data_loader))
    mol_batch, features_batch, target_batch, atom_descriptors_batch = \
        batch_val.batch_graph(), batch_val.features(), batch_val.targets(), batch_val.atom_descriptors()
    target_batch = torch.Tensor(target_batch).to(device)
    with torch.no_grad():
        loss = mod.elbo(mol_batch[0], target_batch, args.batch_size, num_samples=1)
    loss_val.append(loss)

    if loss < best_val_loss:
        torch.save(mod.state_dict(), f"Checkpoints/{save_path}")
    print(f"Epoch {n_iter + 1} loss {loss_sum} val loss {loss}")