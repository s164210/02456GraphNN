# README


# Uncertainty Estimation for Molecular Property Prediction
Welcome to our repo :-) this project was made for 02456 Deep Learning.
We demonstrate three ways to draw predictive samples using neural networks.

![](architecture.png)

## Train 
All the training and results code are in the subfolder chemprop_fix.
To see how models are trained on the LEO Dataset check out Langevin_LEO.ipynb, Chemprop Training (Variational Inference).ipynb, hetero_leo.ipynb.

## Results
To see how we create the figures and results in the report check out Uncertainty Results (LEO Pharma).ipynb or Uncertainty Results (Freesolv).ipynb